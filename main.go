package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/widget"
	"fyne_demo/theme"
	"time"
)

type App struct {
	output *widget.Label
	clock  *widget.Label
}

var myApp = App{}

func main() {
	a := app.New()
	w := a.NewWindow("你好 Fyne")

	fyne.CurrentApp().Settings().SetTheme(&theme.MyTheme{})

	// 系统托盘
	if desk, ok := a.(desktop.App); ok {
		m := fyne.NewMenu("MyApp",
			fyne.NewMenuItem("Show", func() {
				w.Show()
			}))
		desk.SetSystemTrayMenu(m)
	}

	output, entry, btn, clock := myApp.makeUI()

	w.SetContent(container.NewVBox(output, entry, btn))
	w.SetMaster()
	w.Resize(fyne.NewSize(640, 480))

	w2 := a.NewWindow("dialog")
	w2.SetContent(container.NewVBox(clock))
	w2.Resize(fyne.NewSize(320, 120))
	w2.Show()

	go func() {
		for range time.Tick(time.Second) {
			myApp.updateTime()
		}
	}()

	w.SetCloseIntercept(func() {
		w.Hide()
		w2.Hide()
	})
	w.ShowAndRun()
}

func (app *App) makeUI() (*widget.Label, *widget.Entry, *widget.Button, *widget.Label) {
	output := widget.NewLabel("你好 Fyne!")
	entry := widget.NewEntry()
	btn := widget.NewButton("Click Me", func() {
		output.SetText(entry.Text)
	})
	btn.Importance = widget.HighImportance
	clock := widget.NewLabel(time.Now().Format("当前时间: 03:04:05"))

	app.output = output
	app.clock = clock

	return output, entry, btn, clock
}

func (app *App) updateTime() {
	formatted := time.Now().Format("当前时间: 03:04:05")
	app.clock.SetText(formatted)
}
